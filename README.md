# Miniproject6: AWS Lambda with Logging and Tracing in Rust

## Description

This project demonstrates the integration of logging and tracing within a Rust-based AWS Lambda function. It focuses on performing a basic arithmetic operation (timing two numbers) and outputs the products. The project enhances observability by incorporating the `tracing` and `tracing-subscriber` crates for structured, event-based logging, and integrates with AWS X-Ray for detailed tracing of function invocations. All logs and traces are centralized in AWS CloudWatch, providing a unified view of operational metrics.

To send an invoke request, the user can either configure locally and invoke remotely:

```bash
cargo lambda invoke --remote miniproject6 --data-ascii '{ "num1": 5, "num2": 20 }'

```

Or invoke directly through the API URL:

```bash
curl -X POST https://wcgcuo6cll.execute-api.us-west-2.amazonaws.com/Miniproject6\
  -H 'content-type: application/json' \
  -d '{ "num1": 10, "num2": 20 }'
```


## Demo


### CloudWatch & AWS X-Ray Tracing

Invoke tested by AWS Lambda console and shown in Cloud Watch
![](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG56.jpg)
![](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG57.jpg)

### Trace level loggings
When clicking one of the traces listed on AWS X-Ray, specific timelines and a complete log will be exhibited, integrating the functionality we built into our Rust function through crates tracing and tracing-subscriber
![](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG58.jpg)
![](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG59.jpg)



## Steps Walkthrough

### Build Cargo Project

1. Create a new Cargo project for your Lambda function:

    ```bash
    cargo lambda new <YOUR-PROJECT-NAME>
    ```

2. Build your Rust Lambda function in `src/main.rs`.

3. Add `tracing`, `tracing-subscriber`, and relevant dependencies to `Cargo.toml`:

    ```bash
    cargo add tracing tracing-subscriber
    ```

4. Test build your function:

    ```bash
    cargo lambda watch
    ```

5. Test local invocation:

    ```bash
    cargo lambda invoke --data-ascii '{ "num1": 10, "num2": 20 }'
    ```

### AWS Configuration

1. Create a new IAM user in AWS IAM with the following policies attached:
   - `AWSLambda_FullAccess`
   - `IAMFullAccess`
   - `AWSXrayFullAccess`

2. Generate and save your `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and your preferred `AWS_REGION`.

3. Store these configurations in a `.env` file at the root of your project (ensure this file is not pushed to version control).

4. Export the AWS credentials in your terminal:

    ```bash
    export AWS_ACCESS_KEY_ID=your_access_key_id_here
    export AWS_SECRET_ACCESS_KEY=your_secret_access_key_here
    export AWS_REGION=your_aws_region_here
    ```

### Connect to AWS

1. Build and deploy your function to AWS:

    ```bash
    cargo lambda build --release
    cargo lambda deploy
    ```

2. Verify that your function appears in the AWS Lambda console.

### Check Logging and Tracing

1. In the Lambda function page, under the Configuration tab, set up monitoring tools:

    - Activate tracing in AWS X-Ray.
    - Enable enhanced monitoring with CloudWatch Lambda Insights.

2. Test remote invocation:

    ```bash
    cargo lambda invoke --remote miniproject6 --data-ascii '{ "num1": 5, "num2": 20 }'
    cargo lambda invoke --remote miniproject6 --data-ascii '{ "num1": 10, "num2": 20 }'
    ```
![](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG60.jpg)

3. Monitor the function in CloudWatch and view X-Ray traces for detailed insights.

### Deploy with API

1. Create a new API (REST API) in AWS API Gateway and set up a new resource and method (ANY) integrated with your Lambda function.

2. Deploy your API and retrieve the invoke URL to test API invocations.

## Conclusion

This project showcases how to effectively integrate logging and tracing in a serverless AWS Lambda function using Rust, enhancing observability and operational monitoring with CloudWatch and AWS X-Ray.
